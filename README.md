## Example folder
Testing example is stored in Example folder with format xx_yy-filename.ext.

xx, yy is the code of language.

da—Danish	de—German	et—Estonian	el—Greek
en—English	es—Spanish	fi—Finnish	fr—French
hu—Hungarian	is—Icelandic	it—Italian	nl—Dutch
no—Norwegian	pl—Polish	pt—Portuguese	ru—Russian
sv—Swedish	th—Thai

Ex: en_fr-0001.txt is matched if the detector has result [en,fr]

Content of example files is body of emails.

## profiles folder
Required by Google Language Detection

## Using
Build and run LanguageDetection class.

The LanguageDetection scan all files in Example folder. Detect languages of those file, compare with the prefix of file name to count match result.
The result of the prototype is:
- Google Detection: elapsed: xxx (microseconds), match xx/yy, un-match files
- Tika Detection: elapsed: xxx (microseconds), match xx/yy, un-match files

