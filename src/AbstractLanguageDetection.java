import org.apache.commons.lang3.time.StopWatch;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

abstract public class AbstractLanguageDetection implements LanguageDetectionService
{
   @NotNull
   protected abstract String getName();

   /**
    * Evaluate the detector and print the result
    * @param directory directory to example folder
    */
   public void evaluate(@NotNull File directory)
   {
      StopWatch stopWatch = StopWatch.createStarted();
      stopWatch.suspend();
      int match = 0;
      int total = 0;
      Map<String, String> unMatch = new HashMap<>();
      for (File example: Objects.requireNonNull(directory.listFiles()))
      {
         if (example.isDirectory())
            continue;
         total++;

         List<String> expectedLang = new ArrayList<>(Arrays.asList(example.getName().split("-")[0].split("_")));

         try
         {
            String content = new String(Files.readAllBytes(Paths.get(example.getAbsolutePath())));
            stopWatch.resume();
            if (content.isEmpty())
               throw new IOException("No content");
            List<String> languageResults = detectAll(content);
            stopWatch.suspend();
            for(String languageResult: languageResults)
               expectedLang.remove(languageResult);
            if (expectedLang.isEmpty())
               match++;
            else
            {
               unMatch.put(example.getName(), String.join(",", languageResults));
            }
         }
         catch (IOException e)
         {
            stopWatch.suspend();
            unMatch.put(example.getName(), e.getMessage());
         }
      }
      System.out.println(getName() + " result");
      System.out.println("Elapsed time (ms):" + stopWatch.getTime());
      System.out.println("Match: " + match + "/" + total);
      if (!unMatch.isEmpty())
      {
         System.out.println("Un-match files:");
         unMatch.forEach((name, reason) -> System.out.println(name + ": " + reason));
      }
   }
}
