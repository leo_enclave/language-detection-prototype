import com.cybozu.labs.langdetect.LangDetectException;

import java.io.*;

public class LanguageDetection
{
   private final static String EXAMPLE_DIR = "Example";

   public static void main(String[] args) throws LangDetectException
   {
      File directory = new File(EXAMPLE_DIR);
      if (!directory.exists())
      {
         System.out.println(EXAMPLE_DIR + " does not exist.");
         return;
      }
      if (directory.listFiles() == null)
      {
         System.out.println(EXAMPLE_DIR + " is empty.");
         return;
      }
      new GoogleLanguageDetection().evaluate(directory);
      new TikaLanguageDetection().evaluate(directory);
   }
}
