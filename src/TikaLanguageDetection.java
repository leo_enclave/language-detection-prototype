import org.apache.tika.langdetect.OptimaizeLangDetector;
import org.apache.tika.language.detect.LanguageDetector;
import org.apache.tika.language.detect.LanguageResult;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class TikaLanguageDetection extends AbstractLanguageDetection
{
   private LanguageDetector languageDetector;
   public TikaLanguageDetection()
   {
      languageDetector = new OptimaizeLangDetector().loadModels();
   }

   @NotNull
   public List<String> detectAll(String text)
   {
      List<String> languages = new ArrayList<>();
      List<LanguageResult> languageResults = languageDetector.detectAll(text);
      languageResults.forEach(languageResult -> languages.add(languageResult.getLanguage()));
      return languages;
   }

   @NotNull
   public String detect(String text)
   {
      return languageDetector.detect(text).getLanguage();
   }

   @NotNull
   @Override
   protected String getName()
   {
      return "Tika Language Detection";
   }
}
