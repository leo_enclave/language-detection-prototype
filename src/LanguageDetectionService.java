import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface LanguageDetectionService
{
   @NotNull
   public List<String> detectAll(String text);

   @NotNull
   public String detect(String text);
}
