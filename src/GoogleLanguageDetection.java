import java.util.*;

import com.cybozu.labs.langdetect.Detector;
import com.cybozu.labs.langdetect.DetectorFactory;
import com.cybozu.labs.langdetect.LangDetectException;
import com.cybozu.labs.langdetect.Language;
import org.jetbrains.annotations.NotNull;

class GoogleLanguageDetection extends AbstractLanguageDetection
{
   private final static String PROFILE_DIR = "profiles";

   public GoogleLanguageDetection() throws LangDetectException
   {
      DetectorFactory.loadProfile(GoogleLanguageDetection.PROFILE_DIR);
   }

   @NotNull
   public String detect(String text)
   {
      try
      {
         Detector detector = DetectorFactory.create();
         detector.append(text);
         return detector.detect();
      }
      catch (LangDetectException e)
      {
         e.printStackTrace();
      }
      return "";
   }

   @NotNull
   public List<String> detectAll(String text)
   {
      List<String> languages = new ArrayList<>();
      try
      {
         ArrayList<Language> detectLanguages = this.detectAllLanguages(text);
         detectLanguages.forEach(language -> languages.add(language.lang));
      }
      catch (LangDetectException e)
      {
         e.printStackTrace();
      }
      return languages;
   }

   @NotNull
   @Override
   protected String getName()
   {
      return "Google Language Detection";
   }

   private ArrayList<Language> detectAllLanguages(String text) throws LangDetectException
   {
      Detector detector = DetectorFactory.create();
      detector.append(text);
      return detector.getProbabilities();
   }
}