COLOMBIA
La larga lista de demandas de quienes exigen cambios en Colombia
Colombia cumple una semana de protestas. Manifestaciones que a pesar de ofertas del diálogo brindadas por el gobierno de Iván Duque, no quieren cesar.
El llamado a movilizarse llega desde muchos rincones de la sociedad. Convocan sindicatos, gremios, organizaciones de mujeres, de defensa a los derechos humanos y por supuesto, las asociaciones estudiantiles.  La primera marcha del 21 de noviembre, se convocó bajo el lema "Contra el Paquetazo de Duque, la OCDE, el FMI y el Banco Mundial. Por la vida y la paz”. Un título que logró movilizar a cientos de miles colombianos disconformes con el rumbo del país.

Dilan, símbolo de la represión contra la protesta social

"Estamos cansados de siempre estar divididos. Ha llegado la hora de unirnos por la dignidad", dice Juliana Gaitana, una joven estudiante de derecho, en el segundo día de paro nacional. "En Colombia siempre ha sido unos contra otros: izquierda contra derecha, guerrilla contra paramilitares…ahora petristas contra uribistas y manifestantes contra el ESMAD. Queremos unidad".