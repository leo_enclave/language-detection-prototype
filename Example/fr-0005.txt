VU D'ALLEMAGNE
L'Allemagne veut augmenter sa contribution à l'OTAN
La promesse de l'Allemagne de consacrer plus d’argent aux dépenses militaires fait réagir les éditorialistes. Mais aussi, bien sûr, les défis de la nouvelle Commission européenne validée ce mercredi par les eurodéputés.
"Angela Merkel doit encore convaincre"- (Süddeutsche Zeitung)
Elle faisait presque pitié, mais ça a finalement réussi à Ursula von der Leyen. Le Parlement européen a validé son équipe de commissaires ! s'exclame la Tageszeitung. Mais devant, se dresse un mandat qui s'annonce difficile prévient la TAZ. La Commission a beau avoir obtenu un large soutien des eurodéputés, la présidente de la Commission européenne avait auparavant été choisie à une majorité très mince.

Les longs désaccords autour de la nomination des commissaires ont aussi montré qu'Ursula von der Leyen bénéficie d'une faible légitimité. Cela dit, l'attente, aussi longue soit-elle, a une fin.



 Belgien | Von der Leyen begrüßt Orban bei der Europäischen Kommission (picture-alliance/dpa/Bildfunk/Europäische Kommission/J. Jacquemart)
La présidente de la Commission européenne Ursula von der Leyen salue le président hongrois eurosceptique Viktor Orban (01.08.19)

Les nationalistes à la manoeuvre

En fin de compte, prédit le Tagesspiegel de Berlin, ce sont les gouvernements nationalistes qui auront la main sur la Commission européenne. Cela commencera par la répartition des postes budgétaires de la période allant de 2021 à 2027 et cela finira sur le climat.

Car en définitive, la décision de la mise en œuvre des plans ambitieux annoncés par la présidente von der Leyen se prendra dans les capitales.

Pour la Frankfurter Allgemeine Zeitung, toutes ces difficultés ont leur origine dans le choix de la nouvelle présidente de la Commission européenne sortie de nulle part et imposée par les Etats au détriment des candidats des partis. Ursula von der Leyen devra faire toutes sortes de manœuvres pour mener à bien ses projets les plus importants, conclut la FAZ.