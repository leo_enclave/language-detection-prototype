Konservativer Lacalle Pou ist neuer Präsident Uruguays
Das Ergebnis der Stichwahl ist knapp aber eindeutig: Die Geschicke des lateinamerikanischen Landes lenkt künftig der 46-jährige Luis Lacalle Pou von der rechtsgerichteten Nationalpartei. Er war als Erneuerer angetreten.
Der künftige Präsident Luis Lacalle Pou mit seiner Frau am vergangenen Sonntag

Noch vor Bekanntgabe des offiziellen Wahlergebnisses hat der Kandidat der regierenden Linkspartei, Daniel Martínez, seine Wahlniederlage gegen seinen Rivalen von der rechtsgerichteten Nationalpartei eingeräumt. "Wir grüßen den gewählten Präsidenten Luis Lacalle Pou. Morgen werde ich mich mit ihm treffen", schrieb Martínez im Kurzbotschaftendienst Twitter.