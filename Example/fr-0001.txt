AFRIQUE
Jean-Yves Le Drian prône la pression au Burkina Faso pour rassembler la nation
Le ministre français des Affaires étrangères n'a certes cité aucun nom, mais les regards se tournent vers l'ex-président burkinabé Blaise Compaoré. Le débat enfle sur les conditions d'une réconciliation au Burkina Faso.
"Plus d'action politique, plus de pression politique (aussi) au Burkina Faso où il importe que les autorités puissent rassembler la nation contre les risques", a déclaré le chef de la diplomatie française Jean-Yves Le Drian qui s'exprimait devant la Commission des affaires étrangères de l’Assemblée nationale après la mort de treize militaires français (25.11.) dans une opération antidjihadiste au Mali.

Le Burkina Faso, frontalier du Mali et du Niger, est aussi régulièrement le théâtre d’attaques djihadistes sanglantes contre ses forces de défense. Face à cette situation, certaines voix s'élèvent, demandant le retour d'exil de l'ex-président Blaise Compaoré toujours en Côte d'Ivoire depuis son départ du pouvoir.

A l'occcasion d'une interview exclusive avec le président Roch Marc Christian Kaboré, la DW a posé la question du retour de Compaoré ainsi que ses liens présumés avec les djihadistes. Retrouvez ici un extrait de cette interview !