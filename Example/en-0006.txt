Conscription in Germany: Merkel's party mulls return of military service
Should school leavers spend a year doing national service? The new leader of the Christian Democrats has cautiously floated the idea. Other European countries have reintroduced conscription, for a variety of reasons.
German Defense Minister Annegret Kramp-Karrenbauer, still the current favorite to succeed Angela Merkel as chancellor, has cautiously come out in favor of opening the debate on reintroducing national military service.

"Something is in danger of being lost without which a society cannot continue forever," the leader of the conservative Christian Democratic Union (CDU) told the Funke Media Group. "For me the community within the population is the priority."

She added that she had heard much support for the idea of national service in her "listening tour" of grassroots CDU branches that followed her election as the new party leader. "I share many of these thoughts, and I think it's very important that we should discuss an obligatory national service in Germany," she said.

The issue is being addressed in CDU headquarters in Berlin on Thursday, with invited experts invited to a "workshop talk" on whether a one-year period of national service should be re-introduced for high school graduates.

Read more: Angela Merkel's party unites on future questions