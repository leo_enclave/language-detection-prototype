Roskilde chokerer: Hyrer popgigant
Thomas Helmig er overraskende tilbage på festivalen, som også giver debut til veteranens søn Hugo
Programchef for Roskilde, Anders Wahrén, fortæller om dagens store offentliggørelser. Producer/Redigering: Morten Ildal/Maya Westander
Den havde vi ikke set komme.

Roskilde Festival har netop afsløret, at Thomas Helmig optræder på Orange næste sommer.

Se også: Kæmpe fiasko for Thomas Helmig

Det bliver popstjernens første koncert på festivalen siden 2002. Han debuterede på Dyrskuepladsen tilbage i 1986 med Thomas Helmig Brothers.

I 2020, hvor Roskilde fylder 50, synger den 55-årige jyde sine hits på Orange.

Se også: Helmig i åbent opgør mod sin søn

Arrangørerne har dog også skrevet kontrakt med en sanger, der er mere i øjenhøjde med festivalens unge kernepublikum.

Thomas Helmigs søn, Hugo, debuterer nemlig i Roskilde, efter han meldte afbud i år på grund af sin behandling for kokainmisbrug.