Så er den gal igen.

Ekstra Bladet har de seneste år nøje beskrevet, hvordan hygiejnen ikke altid ligefrem er gourmet-agtig i det eksklusive danske bøf-imperium MASH.

Der er tidligere fundet muggent kød, skimmel, spindelvæv, og en rottehund har lugtet rotter i filialen i Aarhus. Oksehøjreb er blevet opbevaret under åben himmel frit tilgængeligt for skadedyr, og ofte har der slet og ret været snavset.

Gang på gang har MASH lovet, at hygiejnen skal være i top, men alene i denne måned har Fødevarekontrollen to gange grebet ind mod dårlig hygiejne.


Kup - 20. maj. 2015 - kl. 10:49
Mash skjulte kritisk kontrolrapport
I går onsdag fik MASH i den eksklusive Bredgade i København så en sanktion, fordi de køleskabe, hvor det dyre rå kød var udstillet for gæsterne, var beskidte.