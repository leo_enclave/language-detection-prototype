INTERVIEWS EXCLUSIVES
"Misère et grandeur de la liberté d’informer", Eric Topona raconte son histoire
Dans ce livre, le journaliste raconte les déboires des hommes de médias. Parti de son histoire personnelle, Eric Topona plaide pour la liberté d’expression.

"Les journalistes doivent être indépendants" (Eric Topona)
Journaliste à la rédaction Afrique francophone de la Deutsche Welle, Eric Topona a publié le 30 octobre  2019 aux Éditions Edilivres à Paris un ouvrage intitulé : "Misère et grandeur de la liberté d’informer". Le livre est préfacé par Peter Limbourg, le directeur général de la Deutsche Welle.